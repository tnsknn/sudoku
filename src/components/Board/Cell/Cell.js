import React from 'react';
import Dialpad from './Dialpad/Dialpad.js';

class Cell extends React.Component {
    // TODO: cell activity state for opening and closing dialpad ???
    constructor(props) {
        super(props);
        this.state = {
            showDialpad: false
        };
    }
    _toggleDialpad() {
        if (this.state.showDialpad) {
            this.setState({ showDialpad: false})

        } else {
            this.setState({ showDialpad: true})
        }
    }
    _setUserValue(value) {
        // function is passed as a prop to Dialpad in the _renderCell function
        // userValue function in Dialpad returns value
        console.log('cell saa arvon ' + value)
    }
    _renderCell() {

        // initialize dialpad variable
        let dialpad;

        // if showDialpad returns true, show dialpad, else null
        if (this.state.showDialpad) {
            // userValue prop is passed to Dialpad
            // userValue has propType of function
            // Cell's ._setUserValue function is bound to the prop
            dialpad = <Dialpad userValue={this._setUserValue.bind(this)} />
        } else {
            dialpad = null;
        }

        // by default cell is filled with value from array
        // TODO: only numbers
        let cellValue = <div className="sudokuValue fixedValue">{this.props.value}</div>;
        
        // if value is 0 (empty) render cell as empty
        // TODO: figure out what bind(this) does and why does it work
        if (this.props.value === 0) {
            cellValue = (
                        <div className="sudokuValue userValue" onClick={this._toggleDialpad.bind(this)}>
                            {this.props.userValue}
                        </div>
            );
        }

        return (
            <td className="sudokuCell">
                {cellValue}
                {dialpad}
            </td>
        )
    }
    render() {
        return (
            this._renderCell()
        )
    }
};
Cell.propTypes = {
    value: React.PropTypes.number,
    row: React.PropTypes.number,
    col: React.PropTypes.number
};
Cell.DefaultProps = {
    value: 0,
    col: 0,
    row: 0
};

export default Cell;
