import React from 'react';
import Cell from './Cell/Cell.js';

class Board extends React.Component {
    _renderRow(row, rowIndex) {
        // Goes through all the items in a row and renders a cell with its value
        console.log('rowIndex' + rowIndex + ": " + row);
        console.log(row.map((value, colIndex) => <Cell row={rowIndex} col={colIndex} value={value}/>.props));

        return (
            <tr key={rowIndex} className="sudokuRow">
                {
                    row.map( function(value, colIndex) {
                        return <Cell key={colIndex} row={rowIndex} col={colIndex} value={value}/>
                    })
                }
            </tr>
        )
    }
    _renderBoard() {
        // Goes through all the rows and renders the board
        return (
            <table className="sudokuBoard">
                <tbody>
                    {
                        this.props.board.map((row, rowIndex) => this._renderRow(row, rowIndex))
                    }
                </tbody>
            </table>
        )
    }
    render() {
        return (
            this._renderBoard()
        )
    }
}
Board.propTypes = {
    board: React.PropTypes.array
};
Board.defaultProps = {
    // Default board properties
    // TODO: Randomize the board or use hand crafted boards, sudoku logic library?
    // TODO: Difficulty setting?

    // Easy sudoku board grabbed from Google Images
    board: [
        [ 0, 6, 0, 0, 1, 0, 0, 0, 2 ],
        [ 0, 4, 0, 6, 2, 0, 7, 0, 0 ],
        [ 0, 0, 0, 3, 0, 7, 0, 8, 0 ],
        [ 0, 2, 0, 0, 4, 0, 9, 0, 0 ],
        [ 0, 0, 1, 0, 0, 0, 2, 0, 0 ],
        [ 0, 0, 3, 0, 8, 0, 0, 5, 0 ],
        [ 0, 9, 0, 8, 0, 4, 0, 0, 0 ],
        [ 0, 0, 4, 0, 3, 2, 0, 9, 0 ],
        [ 2, 0, 0, 0, 7, 0, 0, 4, 0 ],
    ]
};

export default Board;
