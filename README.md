# pls read me

## How do I even

1. Install NodeJS & npm
2. Clone/checkout the repo
3. `npm install` to install packages found in package.json
4. New terminal window: `npm start` in the project folder
5. Project should be live at [localhost:3000](localhost:3000)
6. New terminal window: `npm run watch-css` to compile Sass to CSS
7. HOPE IT WORKS GL


## Good to know

Built using Node v7.0.0 & npm 3.10.9

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). Read  [documentation](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

### Includes the following
- Bootstrap 4
- Sass

### Hopefully this
- Works
- Is finished someday