import React from 'react';
import Dialbutton from './Dialbutton/Dialbutton.js';

class Dialpad extends React.Component {
    _getUserValue(event) {
        // function takes event as a parameter
        // event is passed through the userValue prop function
        console.log('painettiin ' + event.target.value)

        this.props.userValue(event.target.value);
    }
    _renderDialpadRow(row, rowIndex) {
        // getUserValue is a function that is passed to Dialbutton as a prop
        // getUserValue is bound to Dialpad's function _getUserValue
        let getUserValue = this._getUserValue.bind(this)
        return (
            <tr key={rowIndex} className="dialpadRow">
                {
                    row.map( function(value, colIndex) {

                        let dialbutton = <Dialbutton key={colIndex} row={rowIndex} col={colIndex} value={value} userValue={getUserValue}/>

                        return (
                            dialbutton
                        )
                    })
                }
            </tr>
        )
    }
    _renderDialpad() {
        // when Dialpad is opened this value is passed to Cell's _setUserValue function through the userValue prop which is a function as well
        let value = 'hello dialpad';
        this.props.userValue(value);

        return (
            <table className="sudokuDialpad">
                <tbody>
                    {
                        this.props.dialpad.map((row, rowIndex) => this._renderDialpadRow(row, rowIndex))
                    }
                </tbody>
            </table>
        )
    }
    render() {
        return (
            this._renderDialpad()
        )
    }
}
Dialpad.propTypes = {
    dialpad: React.PropTypes.array,
    userValue: React.PropTypes.func
};
Dialpad.defaultProps = {
    dialpad: [
        [ 1, 2, 3 ],
        [ 4, 5, 6 ],
        [ 7, 8, 9 ]
    ]
};

export default Dialpad;
