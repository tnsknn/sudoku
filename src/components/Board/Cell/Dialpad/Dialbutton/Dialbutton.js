import React from 'react';

class Dialbutton extends React.Component {
    _renderDialbutton() {
        // populate buttons with numbers
        // onClick function is handled in Dialpad component by _getUserValue function
        // userValue here is a function that is passed as a prop from Dialpad (the parent)
        let dialbutton = <button className="btn btn-block dialbutton" onClick={this.props.userValue} value={this.props.value}>{this.props.value}</button>;

        return (
            <td className="dialpadCell">
                {dialbutton}
            </td>
        )
    }
    render() {
        return (
            this._renderDialbutton()
        )
    }
}
Dialbutton.propTypes = {
    userValue: React.PropTypes.func,
    value: React.PropTypes.number,
    row: React.PropTypes.number,
    col: React.PropTypes.number
};
Dialbutton.defaultProps = {
    value: 0,
    col: 0,
    row: 0
};

export default Dialbutton;
