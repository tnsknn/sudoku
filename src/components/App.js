import React from 'react';
import Board from './Board/Board.js';

class App extends React.Component {
    render() {
        return (
            <div className="container">
                <header>
                    <h1>Best sudoku</h1>
                </header>
                <Board />
            </div>
        )
    }
}

export default App;
