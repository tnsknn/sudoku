import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

ReactDOM.render(
  <App name="sudoku"/>,
  document.getElementById('app')
);
